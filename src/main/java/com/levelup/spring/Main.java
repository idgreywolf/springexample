package com.levelup.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {


    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("example.xml");
        UserService userService = (UserService) applicationContext.getBean("userService");
        UserService1 userService1 = (UserService1) applicationContext.getBean("userService1");
        System.out.println(userService.getString());
        System.out.println(userService.getString2());
        System.out.println();
    }


}
