package com.levelup.spring.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean(name = "string")
    public String getString() {
        return "TestString";
    }

    @Bean(name = "string2")
    public String getString2() {
        return "TestString 2";
    }
}
