package com.levelup.spring;

public interface Reference {

    void print();
}
