package com.levelup.spring;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Setter
@Getter
@Repository
public class UserRepository implements Reference {

    private String text;


    public void print() {
        System.out.println(text);
    }
}
